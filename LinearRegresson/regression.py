import numpy as np
import matplotlib.pyplot as plt # type: ignore


def compute_model_output(x, w, b):
    """
    Computes the prediction of a linear model
    Args:
      x (ndarray (m,)): Data, m examples 
      w,b (scalar)    : model parameters  
    Returns
      f_wb (ndarray (m,)): model prediction
    """
    m = x.shape[0]
    f_wb = np.zeros(m)
    for i in range(m):
        f_wb[i] = w * x[i] + b
        
    return f_wb

#plt.style.use('./deeplearning.mplstyle')

# x_train is the input variable (size in 1000 square feet)
# y_train is the target (price in 1000s of dollars)
x_train = np.array([1.0, 2.0])
y_train = np.array([300.0, 500.0])
print("x_train = {x_train}")
print("y_train = {y_train}")


# m is the number of training examples
print("x_train.shape: {x_train.shape}")
m = x_train.shape[0]
print("Number of training examples is: {m}")


# m is the number of training examples
m = len(x_train)
print("Number of training examples is: {m}")


# # Plot the data points
# plt.scatter(x_train, y_train, marker='x', c='r')
# # Set the title
# plt.title("Housing Prices")
# # Set the y-axis label
# plt.ylabel('Price (in 1000s of dollars)')
# # Set the x-axis label
# plt.xlabel('Size (1000 sqft)')
# plt.show()


w = 100
b = 100
print("w: {w}")
print("b: {b}")

# tmp_f_wb = compute_model_output(x_train, w, b,)

# # Plot our model prediction
# plt.plot(x_train, tmp_f_wb, c='b',label='Our Prediction')

# # Plot the data points
# plt.scatter(x_train, y_train, marker='x', c='r',label='Actual Values')

# # Set the title
# plt.title("Housing Prices")
# # Set the y-axis label
# plt.ylabel('Price (in 1000s of dollars)')
# # Set the x-axis label
# plt.xlabel('Size (1000 sqft)')
# plt.legend()
# plt.show()


tmp_f_wb = compute_model_output(x_train, w, b,)

# Plot our model prediction
plt.plot(x_train, tmp_f_wb, c='b',label='Our Prediction')

# Plot the data points
plt.scatter(x_train, y_train, marker='x', c='r',label='Actual Values')

# Set the title
plt.title("Housing Prices")
# Set the y-axis label
plt.ylabel('Price (in 1000s of dollars)')
# Set the x-axis label
plt.xlabel('Size (1000 sqft)')
plt.legend()
plt.show()

